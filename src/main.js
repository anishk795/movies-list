import Vue from 'vue';
import axios from 'axios';
import _ from 'lodash';

import BootstrapVue from 'bootstrap-vue';

import App from './App.vue'
// BootStrap imports
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue);
Vue.use(axios);
Vue.use(_);

new Vue({
  render: h => h(App)
}).$mount('#app')
